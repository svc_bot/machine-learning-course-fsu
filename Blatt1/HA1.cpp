#include <iostream>

float diff(int iter)
{
	float forward = 0, backward = 0, dif = 0;

	for (int i = 1; i < iter+1; ++i)
	{
		forward += 1.0/i;
	}

	for (int i = iter; i > 0; --i)
	{
		backward += 1.0/i;
	}

	dif = forward - backward;
	if (dif > 0.0) return dif;
	else return -dif;
}

int main()
{
	std::cout << diff(10000) << std::endl;

	return 0;
}

