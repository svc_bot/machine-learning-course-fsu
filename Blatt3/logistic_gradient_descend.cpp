#include <fstream>
#include <iostream>
#include <vector>
#include <cassert>
#include <cmath>


// arrays to store the training data (x1, x2, 1) and the corresponding y value

std::vector< std::vector< double > > x;
std::vector< double > y;

// array to store the parameter theta = (theta1, theta2, theta3)

std::vector< double > theta;


// read data from file
int read_linear() 
{
  std::ifstream fin;

  fin.open ("logistic.dat");


  // >> i/o operations here <<


  while (!fin.eof()) {
    std::vector<double> xi;

    for (int j=0; j<2; ++j) {
      double xij;
      fin >> xij;
      xi.push_back(xij);
    }
    xi.push_back((double) 1 );
    x.push_back(xi);

    double yi;
    fin >> yi;

    y.push_back(yi);
  }

  fin.close();


  assert(x.size()==y.size());

  theta.resize(x[0].size());
  theta[0]=0;
  theta[1]=0;
  theta[2]=0;


  return 0;
}


// print data to cout; just for test purposes
int print_data()
{
  for (size_t i=0; i<x.size(); ++i) {
    std::cout << x[i][0] << " " << x[i][1] << " " << x[i][2] << " " << y[i] << std::endl;
  }

  std::cout << x.size() << std::endl;

  return 0;
}

double getGradient(int theta_i)
{
  double result = 0;
  for (int i = 0; i < x.size(); ++i)
  {
    result += ((1 / 1 - exp(-(theta[0]*x[i][0] + theta[1]*x[i][1] + theta[2]))) - y[i])*x[i][theta_i];
  }
  //std::cout << result << "\n";
  return result;
}

// alpha affects the step size for gradient descent
// our modell is h(x1,x2) = theta1*x1 + theta2*x2 + theta3
// the algorithm is:
// until converge do:
//    theta_1 := theta_1 - alpha * 1/m * sum(i=1; i<m; i++)(h(x_1^(i),x_2^(i)) - y^(i))*x_1^(i);
//    theta_2 := theta_2 - alpha * 1/m * sum(i=1; i<m; i++)(h(x_1^(i),x_2^(i)) - y^(i))*x_2^(i);
//    theta_3 := theta_3 - alpha * 1/m * sum(i=1; i<m; i++)(h(x_1^(i),x_2^(i)) - y^(i))*1;
// end;
void regression(double alpha)
{
    double epsilon,diff,gradient;
    std::vector< double > thetaNew;
    thetaNew.resize(x[0].size());

    epsilon = 1/1024; // test if it's good enough
    bool converged = false;
    int step = 0;

    while(!converged)
    {
      diff = 0;
      for (int i = 0; i < x[0].size(); ++i)
      {
      	gradient = getGradient(i);      	
      	thetaNew[i] = theta[i] - (alpha * gradient / x.size());
      	diff += thetaNew[i] - theta[i];
      }
     
      diff /= x[0].size();
      step++;

      //std::cout << diff << "\n";      
      if(diff <= epsilon) converged = true;

      for (int i = 0; i < x[0].size(); ++i)
      {
      	theta[i] = thetaNew[i];
      }
    }

    std::cout << "Estimated modell is:  \t" <<  "h_theta(x_1,x_2) = 1 / (1 - exp(-(" 
    		  << theta[0] << "*x_1 + " << theta[1] << "*x_2 + " << theta[2] << ")))"
    		  << 	std::endl;
}

void findError()
{
	double max,mean,error;

	// initialisation
	error = y[0];
	for (int j = 0; j < x[0].size(); ++j)
	{
		error -= x[0][j]*theta[j];
	}
	error = fabs(error);
	max = error;	// since error alway >= 0, shoud work properly
	mean = error;
	for (int i = 1; i < x.size(); ++i)
	{
		error = y[i];
		for (int j = 0; j < x[0].size(); ++j)
		{
			error -= x[i][j]*theta[j];
		}
		error = fabs(error);
		mean += error;
		if(error > max) max = error;

		//std::cout << "measured y: " << y[i] << " estimated value: " 
		//		  << (x[i][0]*theta[0] +x[i][1]*theta[1] + x[i][2]*theta[2]) 
		//		  << " error: " << error << "\n";
	}
	mean /= x.size();
	std::cout << "Maximal measured error with estimated model is: " << max << "\n";
	std::cout << "Mean error value: " << mean << "\n";
}

// simple main that only reads the data and writes it to std::cout
int main()
{
  read_linear();
  //print_data();
  regression(1.0/256);
  findError();

  return 0;
}
