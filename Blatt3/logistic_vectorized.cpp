#include <cassert>

#include <exception>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>

#include <eigen_lib/Eigen/Core>

static std::size_t const DIM = 2;
static char FILENAME[] = "logistic.dat";

typedef double NumberType;
typedef Eigen::Matrix<NumberType, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
typedef Eigen::Matrix<NumberType, Eigen::Dynamic, 1> Vector;

// read data from file
void read_linear(Matrix & X, Vector & y) {
  typedef std::vector<NumberType> tmp_vector;
  std::vector<tmp_vector> X_tmp;
  tmp_vector y_tmp;

  std::ifstream fin(FILENAME);
  if (!fin.is_open())
    throw std::invalid_argument("error while reading data file");

  // >> i/o operations here <<
  std::string line;
  while (std::getline(fin, line)) {
    std::istringstream line_stream(line);
    // insert a new vector
    X_tmp.push_back(tmp_vector(DIM + 1));
    tmp_vector & x_i = X_tmp.back();
    x_i[DIM] = 1;  // the last coordinate is always 1
    for (std::size_t j = 0; j < DIM; ++j)
      line_stream >> x_i[j];

    // insert a value
    y_tmp.push_back(NumberType());
    NumberType & y_i = y_tmp.back();
    line_stream >> y_i;
  }
  // there should be as many vectors as there are target values
  assert(X_tmp.size() == y_tmp.size());

  // allocate Eigen Matrix and Vector and fill them with data
  X.resize(X_tmp.size(), DIM + 1);
  for (std::size_t i = 0; i < X_tmp.size(); ++i)
    X.row(i) = Eigen::Map<Vector>(X_tmp[i].data(), X_tmp[i].size()).transpose();

  y = Eigen::Map<Vector>(y_tmp.data(), y_tmp.size());
}

// print data to cout; just for test purposes
void print_data(Matrix const& X, Vector const& y) {
  assert(X.rows() == y.rows());
  for (int i = 0; i < X.rows(); ++i) {
    // print the x_i
    std::cout << X.row(i) << ' ';
    // print the y_i
    std::cout << y[i] << '\n';
  }
  std::cout << "data size: " << X.rows() << std::endl;
}

void regression(Matrix const& X, Vector const& y,
                Vector & theta) {

// Eigen example usage
// for more details see http://eigen.tuxfamily.org/dox/QuickRefPage.html

/*
    Matrix A = Matrix::Random(2, 3);  // a random 2x3 matrix
    Vector x(3); x << 1 , 2 , 3;      // a vector with fixed coefficients

    std::cout << "A = " << A << '\n';
    std::cout << "x^T = " << x.transpose() << '\n';

    Vector c = A * x;  // simple matrix-vector product
    std::cout << "c^T = " << c.transpose() << '\n';
    std::cout << "sum of c's components = " << c.sum() << std::endl;
    std::cout << "norm of c = " << c.norm() << std::endl;
    std::cout << "squared norm of c = " << c.squaredNorm() << std::endl;
*/

  // input checks
  assert(X.rows() == y.rows());
  assert(X.cols() == theta.size());

}

// simple main that only reads the data and writes it to std::cout
int main() {
  // arrays to store the training data (x1, x2, 1) and the corresponding y value
  Matrix X;
  Vector y;
  // array to store the parameter theta = (theta1, theta2, theta3)
  Vector theta = Vector::Zero(DIM + 1);

  try {
    read_linear(X, y);
  } catch (std::exception const& e) {
    std::cerr << e.what() << '\n';
    exit(EXIT_FAILURE);
  }

//  print_data(X, y);
  regression(X, y, theta);
  std::cout << "theta = " << theta.transpose() << std::endl;

  exit(EXIT_SUCCESS);
}
